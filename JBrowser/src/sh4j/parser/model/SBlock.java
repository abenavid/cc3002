package sh4j.parser.model;

import java.util.ArrayList;
import java.util.List;

import sh4j.model.format.SFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class SBlock.
 */
public class SBlock extends SText {

	/** The texts. */
	private List<SText> texts;

	/**
	 * Instantiates a new s block.
	 */
	public SBlock() {
		texts = new ArrayList<SText>();
	}

	/**
	 * Space.
	 */
	public void space() {
		texts.add(new SSpace());
	}

	/**
	 * Enter.
	 */
	public void enter() {
		texts.add(new SCarriageReturn());
	}

	/**
	 * Word.
	 *
	 * @param word
	 *            the word
	 */
	public void word(String word) {
		texts.add(new SWord(word));
	}

	/**
	 * Symbol.
	 *
	 * @param c
	 *            the c
	 */
	public void symbol(char c) {
		texts.add(new SChar(c));
	}

	/**
	 * Block.
	 *
	 * @return the s block
	 */
	public SBlock block() {
		SBlock block = new SBlock();
		texts.add(block);
		return block;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.parser.model.SText#export(sh4j.model.format.SFormatter)
	 */
	@Override
	public void export(SFormatter format) {
		format.styledBlock(this);
	}

	/**
	 * Texts.
	 *
	 * @return the list
	 */
	public List<SText> texts() {
		return texts;
	}
}
