package sh4j.parser.model;

import sh4j.model.format.SFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class SWord.
 */
public class SWord extends SText {

	/** The word. */
	private String word;

	/**
	 * Instantiates a new s word.
	 *
	 * @param word
	 *            the word
	 */
	public SWord(String word) {
		this.word = word;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.parser.model.SText#export(sh4j.model.format.SFormatter)
	 */
	@Override
	public void export(SFormatter format) {
		format.styledWord(word);
	}
}
