package sh4j.parser.model;

import sh4j.model.format.SFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class SCarriageReturn.
 */
public class SCarriageReturn extends SText {

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.parser.model.SText#toString()
	 */
	public String toString() {
		return "\n";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.parser.model.SText#export(sh4j.model.format.SFormatter)
	 */
	@Override
	public void export(SFormatter format) {
		format.styledCR();
	}

}
