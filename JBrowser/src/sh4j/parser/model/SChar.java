package sh4j.parser.model;

import sh4j.model.format.SFormatter;

// TODO: Auto-generated Javadoc
/**
 * The Class SChar.
 */
public class SChar extends SText {

	/** The c. */
	private char c;

	/**
	 * Instantiates a new s char.
	 *
	 * @param c
	 *            the c
	 */
	public SChar(char c) {
		this.c = c;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.parser.model.SText#export(sh4j.model.format.SFormatter)
	 */
	@Override
	public void export(SFormatter format) {
		format.styledChar(c);
	}

}
