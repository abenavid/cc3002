package sh4j.model.command;

import java.util.Collections;
import java.util.Comparator;

import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

// TODO: Auto-generated Javadoc
/**
 * The Class SSortPackagesByName.
 */
public class SSortPackagesByName extends SCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.command.SCommand#executeOn(sh4j.model.browser.SProject)
	 */
	@Override
	public void executeOn(SProject project) {
		Collections.sort(project.packages(), new Comparator<SPackage>() {
			@Override
			public int compare(SPackage o1, SPackage o2) {
				return o1.toString().compareTo(o2.toString());
			}
		});
		project.changed();
	}

}
