package sh4j.model.command;

import java.util.Collections;
import java.util.Comparator;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

// TODO: Auto-generated Javadoc
/**
 * The Class SSortClassesByName.
 */
public class SSortClassesByName extends SCommand {

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.command.SCommand#executeOn(sh4j.model.browser.SProject)
	 */
	@Override
	public void executeOn(SProject project) {

		for (SPackage pkg : project.packages()) {
			Collections.sort(pkg.classes(), new Comparator<SClass>() {
				@Override
				public int compare(SClass o1, SClass o2) {
					return o1.toString().compareTo(o2.toString());
				}
			});
		}
		project.changed();
	}
}