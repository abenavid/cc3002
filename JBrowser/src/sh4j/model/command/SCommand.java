package sh4j.model.command;

import sh4j.model.browser.SProject;

// TODO: Auto-generated Javadoc
/**
 * The Class SCommand.
 */
public abstract class SCommand {

	/**
	 * Execute on.
	 *
	 * @param project
	 *            the project
	 */
	public abstract void executeOn(SProject project);

	/**
	 * Name.
	 *
	 * @return the string
	 */
	public String name() {
		return this.getClass().getName();
	}
}
