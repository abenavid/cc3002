package sh4j.model.highlight;

import java.util.regex.Pattern;

import sh4j.model.style.SStyle;
// TODO: Auto-generated Javadoc

/**
 * Class Name highlighter.
 *
 * @author juampi
 */
public class SClassName implements SHighlighter {

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
	 */
	@Override
	public boolean needsHighLight(String text) {

		return Pattern.compile("[A-Z_$]+[a-zA-Z0-9_$]*").matcher(text).matches();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String,
	 * sh4j.model.style.SStyle)
	 */
	@Override
	public String highlight(String text, SStyle style) {
		return style.formatClassName(text);
	}

}
