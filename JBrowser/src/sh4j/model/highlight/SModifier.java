package sh4j.model.highlight;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import sh4j.model.style.SStyle;

// TODO: Auto-generated Javadoc
/**
 * The Class SModifier.
 */
public class SModifier implements SHighlighter {

	/** The Constant javaModifiers. */
	private static final Set<String> javaModifiers = new HashSet<String>(
			Arrays.asList("private", "protected", "public"));

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.highlight.SHighlighter#needsHighLight(java.lang.String)
	 */
	@Override
	public boolean needsHighLight(String text) {
		return javaModifiers.contains(text);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.highlight.SHighlighter#highlight(java.lang.String,
	 * sh4j.model.style.SStyle)
	 */
	@Override
	public String highlight(String text, SStyle style) {
		return style.formatModifier(text);
	}

}
