package sh4j.model.format;

import sh4j.parser.model.SBlock;

// TODO: Auto-generated Javadoc
/**
 * The Interface SFormatter.
 */
public interface SFormatter {

	/**
	 * Styled word.
	 *
	 * @param word
	 *            the word
	 */
	public void styledWord(String word);

	/**
	 * Styled char.
	 *
	 * @param c
	 *            the c
	 */
	public void styledChar(char c);

	/**
	 * Styled space.
	 */
	public void styledSpace();

	/**
	 * Styled cr.
	 */
	public void styledCR();

	/**
	 * Styled block.
	 *
	 * @param b
	 *            the b
	 */
	public void styledBlock(SBlock b);

	/**
	 * Formatted text.
	 *
	 * @return the string
	 */
	public String formattedText();
}
