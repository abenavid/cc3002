package sh4j.model.format;

import sh4j.model.highlight.SDummy;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SStyle;
import sh4j.parser.model.SBlock;
import sh4j.parser.model.SText;

// TODO: Auto-generated Javadoc
/**
 * The Class SHTMLFormatter.
 */
public class SHTMLFormatter implements SFormatter {

	/** The buffer. */
	private StringBuffer buffer;

	/** The level. */
	private int level;

	/** The style. */
	private SStyle style;

	/** The highlighters. */
	private SHighlighter[] highlighters;

	/** The line count. */
	private int lineCount;

	/**
	 * Instantiates a new SHTML formatter.
	 *
	 * @param style
	 *            the style
	 * @param hs
	 *            the hs
	 */
	public SHTMLFormatter(SStyle style, SHighlighter... hs) {
		this.style = style;
		highlighters = hs;
		buffer = new StringBuffer();
		lineCount = 0;
	}

	/**
	 * Lookup.
	 *
	 * @param text
	 *            the text
	 * @return the s highlighter
	 */
	private SHighlighter lookup(String text) {
		for (SHighlighter h : highlighters) {
			if (h.needsHighLight(text)) {
				return h;
			}
		}
		return new SDummy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#styledWord(java.lang.String)
	 */
	@Override
	public void styledWord(String word) {
		buffer.append(lookup(word).highlight(word, style));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#styledChar(char)
	 */
	@Override
	public void styledChar(char c) {
		buffer.append(lookup(c + "").highlight(c + "", style));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#styledSpace()
	 */
	@Override
	public void styledSpace() {
		buffer.append(' ');
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#styledCR()
	 */
	@Override
	public void styledCR() {

		lineCount++;
		String number = lineCount < 10 ? " " + lineCount : "" + lineCount;
		buffer.append("\n");
		buffer.append("<span style='background:#f1f0f0;'> " + number + " </span>");
		indent();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#styledBlock(sh4j.parser.model.SBlock)
	 */
	@Override
	public void styledBlock(SBlock b) {
		level++;
		for (SText text : b.texts()) {
			text.export(this);
		}
		level--;
	}

	/**
	 * Indent.
	 */
	public void indent() {
		for (int i = 0; i < level; i++)
			buffer.append("  ");
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.format.SFormatter#formattedText()
	 */
	@Override
	public String formattedText() {
		return style.formatBody(buffer.toString());
	}

	/**
	 * Tag.
	 *
	 * @param name
	 *            the name
	 * @param content
	 *            the content
	 * @param style
	 *            the style
	 * @return the string
	 */
	public static String tag(String name, String content, String style) {
		return "<" + name + " style='" + style + "'>" + content + "</" + name + ">";
	}
}
