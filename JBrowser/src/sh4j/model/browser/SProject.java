package sh4j.model.browser;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class SProject.
 */
public class SProject extends SObject {

	/** The packages. */
	private List<SPackage> packages;

	/**
	 * Instantiates a new s project.
	 */
	public SProject() {
		packages = new ArrayList<SPackage>();
	}

	/**
	 * Adds the package.
	 *
	 * @param pack
	 *            the pack
	 */
	public void addPackage(SPackage pack) {
		packages.add(pack);
	}

	/**
	 * Packages.
	 *
	 * @return the list
	 */
	public List<SPackage> packages() {
		return packages;
	}

	/**
	 * Gets the.
	 *
	 * @param pkgName
	 *            the pkg name
	 * @return the s package
	 */
	public SPackage get(String pkgName) {
		for (SPackage pkg : packages) {
			if (pkg.toString().equals(pkgName)) {
				return pkg;
			}
		}
		return null;
	}

}
