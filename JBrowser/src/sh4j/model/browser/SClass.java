package sh4j.model.browser;

import java.awt.Font;
import java.util.List;

import org.eclipse.jdt.core.dom.TypeDeclaration;

// TODO: Auto-generated Javadoc
/**
 * The Class SClass.
 */
public class SClass extends SObject {

	/** The declaration. */
	private TypeDeclaration declaration;

	/** The methods. */
	private List<SMethod> methods;

	/**
	 * Instantiates a new s class.
	 *
	 * @param td
	 *            the td
	 * @param ms
	 *            the ms
	 */
	public SClass(TypeDeclaration td, List<SMethod> ms) {
		declaration = td;
		methods = ms;
	}

	/**
	 * Methods.
	 *
	 * @return the list
	 */
	public List<SMethod> methods() {
		return methods;
	}

	/**
	 * Class name.
	 *
	 * @return the string
	 */
	public String className() {
		return declaration.getName().getIdentifier();
	}

	/**
	 * Checks if is interface.
	 *
	 * @return true, if is interface
	 */
	public boolean isInterface() {
		return declaration.isInterface();
	}

	/**
	 * Super class.
	 *
	 * @return the string
	 */
	public String superClass() {
		if (declaration.getSuperclassType() == null)
			return "Object";
		return declaration.getSuperclassType().toString();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return className();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.browser.SObject#font()
	 */
	@Override
	public Font font() {
		if (isInterface()) {
			return new Font("Helvetica", Font.ITALIC, 12);
		}
		return super.font();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.browser.SObject#icon()
	 */
	public String icon() {
		if (isInterface()) {
			return "./resources/int_obj.gif";
		}
		return "./resources/class_obj.gif";
	}
}