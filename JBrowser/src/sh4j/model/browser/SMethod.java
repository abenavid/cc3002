package sh4j.model.browser;

import java.awt.Color;

import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.Modifier;

import sh4j.parser.model.SBlock;

// TODO: Auto-generated Javadoc
/**
 * The Class SMethod.
 */
public class SMethod extends SObject {

	/** The declaration. */
	private MethodDeclaration declaration;

	/** The body. */
	private SBlock body;

	/**
	 * Instantiates a new s method.
	 *
	 * @param node
	 *            the node
	 * @param body
	 *            the body
	 */
	public SMethod(MethodDeclaration node, SBlock body) {
		declaration = node;
		this.body = body;
	}

	/**
	 * Modifier.
	 *
	 * @return the string
	 */
	public String modifier() {
		for (Object obj : declaration.modifiers()) {
			if (obj instanceof Modifier) {
				Modifier modifier = (Modifier) obj;
				return modifier.getKeyword().toString();
			}
		}
		return "default";
	}

	/**
	 * Name.
	 *
	 * @return the string
	 */
	public String name() {
		return declaration.getName().getIdentifier();
	}

	/**
	 * Body.
	 *
	 * @return the s block
	 */
	public SBlock body() {
		return body;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return name();
	}

	/**
	 * Gets the lines of code.
	 *
	 * @return the lines of code
	 */
	public int getLinesOfCode() {
		String definition = this.body().toString();
		return definition.length() - definition.replace("\n", "").length();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.browser.SObject#icon()
	 */
	public String icon() {
		return "./resources/" + modifier() + "_co.gif";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.browser.SObject#background()
	 */
	@Override
	public Color background() {
		if (getLinesOfCode() > 50) {
			return Color.RED;
		}
		if (getLinesOfCode() > 30) {
			return Color.YELLOW;
		}
		return super.background();
	}
}
