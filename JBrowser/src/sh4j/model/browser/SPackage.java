package sh4j.model.browser;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc
/**
 * The Class SPackage.
 */
public class SPackage extends SObject {

	/** The name. */
	private String name;

	/** The classes. */
	private List<SClass> classes;

	/**
	 * Instantiates a new s package.
	 *
	 * @param name
	 *            the name
	 */
	public SPackage(String name) {
		classes = new ArrayList<SClass>();
		this.name = name;
	}

	/**
	 * Adds the class.
	 *
	 * @param cls
	 *            the cls
	 */
	public void addClass(SClass cls) {
		classes.add(cls);
	}

	/**
	 * Classes.
	 *
	 * @return the list
	 */
	public List<SClass> classes() {
		return classes;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return name;
	}

	/**
	 * Checks if is empty.
	 *
	 * @return true, if is empty
	 */
	public boolean isEmpty() {
		return classes.isEmpty();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see sh4j.model.browser.SObject#icon()
	 */
	public String icon() {
		if (isEmpty()) {
			return "./resources/pack_empty_co.gif";
		}
		return "./resources/package_mode.gif";
	}
}
