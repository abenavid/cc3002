package sh4j.ui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import sh4j.model.browser.SClass;
import sh4j.model.browser.SFactory;
import sh4j.model.browser.SMethod;
import sh4j.model.browser.SObject;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;
import sh4j.model.command.SCommand;
import sh4j.model.highlight.SHighlighter;
import sh4j.model.style.SBredStyle;
import sh4j.model.style.SDarkStyle;
import sh4j.model.style.SEclipseStyle;
import sh4j.model.style.SStyle;

// TODO: Auto-generated Javadoc
/**
 * The Class SFrame.
 */
@SuppressWarnings({ "unchecked", "serial" })
public class SFrame extends JFrame implements Observer {

	/** The lighters. */
	private SHighlighter[] lighters;

	/** The style. */
	protected SStyle style = new SEclipseStyle();

	/** The packages. */
	private JList<SPackage> packages;

	/** The classes. */
	private JList<SClass> classes;

	/** The methods. */
	private JList<SMethod> methods;

	/** The html panel. */
	private JEditorPane htmlPanel;

	/** The file. */
	private JMenu file;

	/** The project. */
	private SProject project;

	/** The cstyle. */
	private JMenu cstyle;

	/** The cnumber. */
	private JMenu cnumber;

	/**
	 * Instantiates a new s frame.
	 *
	 * @param style
	 *            the style
	 * @param lighters
	 *            the lighters
	 */
	public SFrame(SStyle style, SHighlighter... lighters) {
		super("CC3002 Browser");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setSize(450, 750);
		build();
		this.lighters = lighters;
		this.style = style;
		JMenuBar bar = new JMenuBar();
		file = new JMenu("Sort");
		bar.add(file);
		cstyle = new JMenu("Style");
		JMenuItem sES = new JMenuItem("Eclipse");
		sES.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				style(new SEclipseStyle());
			}
		});
		JMenuItem sDS = new JMenuItem("Dark");
		sES.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				style(new SDarkStyle());
			}
		});
		JMenuItem sBS = new JMenuItem("Bred");
		sES.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				style(new SBredStyle());
			}
		});
		cstyle.add(sES);
		cstyle.add(sDS);
		cstyle.add(sBS);
		bar.add(cstyle);
		cnumber = new JMenu("Line Numbers");
		JMenuItem yes = new JMenuItem("Able Line Numbers");
		yes.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				getBottom(true);
			}
		});
		JMenuItem no = new JMenuItem("Enable Line Numbers");
		no.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent event) {
				getBottom(false);
			}
		});
		cnumber.add(yes);
		cnumber.add(no);
		bar.add(cnumber);
		setJMenuBar(bar);
	}

	/**
	 * Adds the commands.
	 *
	 * @param cmds
	 *            the cmds
	 */
	public void addCommands(SCommand... cmds) {
		for (SCommand c : cmds) {
			addCommand(c);
		}
	}

	/**
	 * Adds the command.
	 *
	 * @param c
	 *            the c
	 */
	private void addCommand(final SCommand c) {
		JMenuItem item = new JMenuItem(c.name());
		item.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (project != null) {
					c.executeOn(project);
				}
			}
		});
		file.add(item);
	}

	/**
	 * Update.
	 *
	 * @param list
	 *            the list
	 * @param data
	 *            the data
	 */
	public void update(@SuppressWarnings("rawtypes") JList list, @SuppressWarnings("rawtypes") List data) {
		list.setListData(data.toArray(new SObject[] {}));
		if (data.size() > 0) {
			list.setSelectedIndex(0);
		}
	}

	/**
	 * Builds the.
	 */
	private void build() {
		buildPathPanel();
		methods = new JList<SMethod>();
		addOn(methods, BorderLayout.EAST);
		classes = new JList<SClass>();
		addOn(classes, BorderLayout.CENTER);
		packages = new JList<SPackage>();
		addOn(packages, BorderLayout.WEST);
		htmlPanel = buildHTMLPanel(BorderLayout.SOUTH);
		packages.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (packages.getSelectedIndex() != -1) {
					update(classes, packages.getSelectedValue().classes());
				}
			}
		});
		classes.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (classes.getSelectedIndex() != -1) {
					update(methods, classes.getSelectedValue().methods());
				}
			}
		});
		methods.addListSelectionListener(new ListSelectionListener() {
			@Override
			public void valueChanged(ListSelectionEvent e) {
				if (methods.getSelectedIndex() != -1) {
					SMethod method = methods.getSelectedValue();
					htmlPanel.setText(method.body().toHTML(style, lighters));
				}
			}
		});

	}

	/**
	 * Adds the on.
	 *
	 * @param list
	 *            the list
	 * @param direction
	 *            the direction
	 */
	public void addOn(JList<? extends SObject> list, String direction) {
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setLayoutOrientation(JList.VERTICAL);
		list.setCellRenderer(new SItemRenderer());
		JScrollPane pane = new JScrollPane(list);
		pane.setMinimumSize(new Dimension(200, 200));
		pane.setPreferredSize(new Dimension(200, 200));
		pane.setMaximumSize(new Dimension(200, 200));
		getContentPane().add(pane, direction);
	}

	/**
	 * Builds the path panel.
	 */
	private void buildPathPanel() {
		JPanel pathPanel = new JPanel();
		pathPanel.setLayout(new BorderLayout());
		final JTextField pathField = new JTextField();
		pathField.setEnabled(false);
		JButton browseButton = new JButton("Browse");
		browseButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				chooser.setCurrentDirectory(new java.io.File("."));
				chooser.setDialogTitle("Select a root folder");
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setAcceptAllFileFilterUsed(false);
				if (chooser.showOpenDialog(SFrame.this) == JFileChooser.APPROVE_OPTION) {
					pathField.setText(chooser.getSelectedFile().getPath());
					try {
						project = new SFactory().create(chooser.getSelectedFile().getPath());
						project.addObserver(SFrame.this);
						update(packages, project.packages());
						if (project.packages().size() > 0) {
							packages.setSelectedIndex(0);
						}
					} catch (IOException ex) {

					}
				}
			}
		});
		pathPanel.add(pathField, BorderLayout.CENTER);
		pathPanel.add(browseButton, BorderLayout.EAST);
		getContentPane().add(pathPanel, BorderLayout.NORTH);
	}

	/**
	 * Builds the html panel.
	 *
	 * @param direction
	 *            the direction
	 * @return the j editor pane
	 */
	public JEditorPane buildHTMLPanel(String direction) {
		JEditorPane htmlPanel = new JEditorPane();
		htmlPanel.setEditable(false);
		htmlPanel.setMinimumSize(new Dimension(600, 300));
		htmlPanel.setPreferredSize(new Dimension(600, 300));
		htmlPanel.setMaximumSize(new Dimension(600, 300));
		htmlPanel.setContentType("text/html");
		getContentPane().add(new JScrollPane(htmlPanel, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS,
				JScrollPane.HORIZONTAL_SCROLLBAR_ALWAYS), direction);
		return htmlPanel;
	}

	/**
	 * Update.
	 *
	 * @param project
	 *            the project
	 */
	public void update(SProject project) {
		SPackage pkg = packages.getSelectedValue();
		SClass cls = classes.getSelectedValue();
		SMethod method = methods.getSelectedValue();
		packages.setListData(project.packages().toArray(new SPackage[] {}));
		packages.setSelectedValue(pkg, true);
		if (pkg != null) {
			classes.setListData(pkg.classes().toArray(new SClass[] {}));
			classes.setSelectedValue(cls, true);
			if (cls != null) {
				methods.setListData(cls.methods().toArray(new SMethod[] {}));
				methods.setSelectedValue(method, true);
			}
		}
	}

	/**
	 * Style.
	 *
	 * @param style
	 *            the style
	 */
	public void style(SStyle style) {
		this.style = style;
		SMethod method = methods.getSelectedValue();
		htmlPanel.setText(method.body().toHTML(style, lighters));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.util.Observer#update(java.util.Observable, java.lang.Object)
	 */
	@Override
	public void update(Observable o, Object arg) {
		if (o instanceof SProject) {
			SProject project = (SProject) o;
			update(project);
		}
	}

	/**
	 * Lines of code.
	 *
	 * @return the int
	 */
	public int linesOfCode() {
		int lines = 0;
		ListModel<SPackage> model = packages.getModel();
		int i;
		for (i = 0; i < model.getSize(); i++) {
			SPackage p = model.getElementAt(i);
			for (SClass c : p.classes()) {
				for (SMethod m : c.methods()) {
					lines = lines + m.getLinesOfCode();
				}
			}
		}
		return lines;
	}

	/**
	 * Number of warnings.
	 *
	 * @return the int
	 */
	public int numberOfWarnings() {
		return 0;
	}

	/**
	 * Number of classes.
	 *
	 * @return the int
	 */
	public int numberOfClasses() {
		return classes.getModel().getSize();
	}

	/**
	 * Gets the bottom.
	 *
	 * @param confirm
	 *            the confirm
	 * @return the bottom string
	 */
	private void getBottom(boolean confirm) {
		String s = "";
		if (confirm) {
			s = "Lines of code:" + Integer.toString(linesOfCode()) + "   Classes:" + Integer.toString(numberOfClasses())
					+ "  Warnings:" + Integer.toString(numberOfWarnings());
		}
		JLabel bottom = new JLabel(s, JLabel.BOTTOM);
		getContentPane().add(bottom);
	}
}
