package test;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import sh4j.model.browser.SFactory;
import sh4j.model.browser.SPackage;
import sh4j.model.browser.SProject;

// TODO: Auto-generated Javadoc
/**
 * The Class SFactoryTest.
 */
public class SFactoryTest {

	/** The project. */
	private SProject project;

	/**
	 * Sets the up.
	 *
	 * @throws IOException
	 *             Signals that an I/O exception has occurred.
	 */
	@Before
	public void setUp() throws IOException {
		project = (new SFactory()).create("./src");
	}

	/**
	 * Test package.
	 */
	@Test
	public void testPackage() {
		assertTrue(project.packages().size() > 0);
	}

	/**
	 * Test classses.
	 */
	@Test
	public void testClassses() {
		SPackage pkg = project.get("/test");
		assertTrue(pkg.classes().size() > 0);
	}

}
